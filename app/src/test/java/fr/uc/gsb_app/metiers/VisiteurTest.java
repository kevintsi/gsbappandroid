package fr.uc.gsb_app.metiers;

import org.junit.Test;

import static org.junit.Assert.*;

public class VisiteurTest {

    private Visiteur vis = new Visiteur("a131", "Laurent", "Villachane");
    private String mat = "a131";
    private String nom = "Laurent";
    private String prenom = "Villachane";
    @Test
    public void getVisMatricule() {
        assertEquals(mat,vis.getVisMatricule());
    }

    @Test
    public void getVisNom() {
        assertEquals(nom,vis.getVisNom());
    }

    @Test
    public void getVisPrenom() {
        assertEquals(prenom,vis.getVisPrenom());
    }

    @Test
    public void setVisMatricule() {
        vis.setVisMatricule("a233");
        String expected = "a233";
        assertEquals(expected,vis.getVisMatricule());
    }

    @Test
    public void setVisNom() {
        vis.setVisNom("test");
        String expected = "test";
        assertEquals(expected, vis.getVisNom());
    }

    @Test
    public void setVisPrenom() {
        vis.setVisPrenom("prenom");
        String expected = "prenom";
        assertEquals(expected,vis.getVisPrenom());
    }
}