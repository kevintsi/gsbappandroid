package fr.uc.gsb_app.metiers;

import org.junit.Test;

import static org.junit.Assert.*;

public class RapportVisiteTest {

    RapportVisite rp = new RapportVisite(1, 5, "Bouleau", "Andre", "Un bilan", "2019-05-02", "2019-05-18");
    private int num = 1;
    private int pranum = 5;
    private String nomPra = "Bouleau";
    private String prenomPra = "Andre";
    private String bilan = "Un bilan";
    private String dateVisite = "2019-05-02";
    private String dateRapport = "2019-05-18";
    @Test
    public void getPra_nom() {
        assertEquals(nomPra,rp.getPra_nom());
    }

    @Test
    public void setPra_nom() {
        rp.setPra_nom("Lavache");
        String expected = "Lavache";
        assertEquals(expected,rp.getPra_nom());
    }

    @Test
    public void getPra_prenom() {
        assertEquals(prenomPra,rp.getPra_prenom());
    }

    @Test
    public void setPra_prenom() {
        rp.setPra_prenom("Maxime");
        String expected = "Maxime";
        assertEquals(expected,rp.getPra_prenom());
    }

    @Test
    public void setRap_num() {
        rp.setRap_num(3);
        int expected = 3;
        assertEquals(expected,rp.getRap_num());
    }

    @Test
    public void setPra_num() {
        rp.setPra_num(6);
        int expected = 6;
        assertEquals(expected,rp.getPra_num());
    }

    @Test
    public void setBilan() {
        rp.setBilan("Un nouveau bilan");
        String expected = "Un nouveau bilan";
        assertEquals(expected,rp.getBilan());
    }

    @Test
    public void setRap_dateVisite() {
        rp.setRap_dateVisite("2019-02-06");
        String expected = "2019-02-06";
        assertEquals(expected, rp.getRap_dateVisite());
    }

    @Test
    public void setRap_dateRapport() {
        rp.setRap_dateRapport("2019-03-06");
        String expected = "2019-03-06";
        assertEquals(expected, rp.getRap_dateRapport());
    }

    @Test
    public void getRap_num() {
        assertEquals(num,rp.getRap_num());
    }

    @Test
    public void getPra_num() {
        assertEquals(pranum,rp.getPra_num());
    }

    @Test
    public void getBilan() {
        assertEquals(bilan, rp.getBilan());
    }

    @Test
    public void getRap_dateVisite() {
        assertEquals(dateVisite,rp.getRap_dateVisite());
    }

    @Test
    public void getRap_dateRapport() {
        assertEquals(dateRapport, rp.getRap_dateRapport());
    }
}