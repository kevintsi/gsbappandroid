package fr.uc.gsb_app.metiers;

import org.junit.Test;

import static org.junit.Assert.*;

public class PraticienTest {

    private Praticien pra = new Praticien(3,"Saleix", "Julian");
    private int numPra = 3;
    private String nomPra = "Saleix";
    private String prenomPra = "Julian";

    @Test
    public void getPraNum() {
        assertEquals(numPra, pra.getPraNum());
    }

    @Test
    public void setPraNum() {
        pra.setPraNum(5);
        int expected = 5;
        assertEquals(expected, pra.getPraNum());
    }

    @Test
    public void getPraNom() {
        assertEquals(nomPra, pra.getPraNom());
    }

    @Test
    public void setPraNom() {
        pra.setPraNom("Nom");
        String expected = "Nom";
        assertEquals(expected, pra.getPraNom());
    }

    @Test
    public void getPraPrenom() {
        assertEquals(prenomPra, pra.getPraPrenom());
    }

    @Test
    public void setPraPrenom() {
        pra.setPraPrenom("Prenom");
        String expected = "Prenom";
        assertEquals(expected, pra.getPraPrenom());
    }
}