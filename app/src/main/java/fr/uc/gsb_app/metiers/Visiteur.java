package fr.uc.gsb_app.metiers;

/**
 * Le type Visiteur.
 */
public class Visiteur
{
    private String visMatricule;
    private String visNom;
    private String visPrenom;

    public Visiteur(String vismat, String visnom, String visprenom){
        this.visMatricule = vismat;
        this.visNom = visnom;
        this.visPrenom = visprenom;
    }

    /**
     * Obtient le matricule du visiteur.
     *
     * @return le matricule du visiteur
     */
    public String getVisMatricule() {
        return visMatricule;
    }

    /**
     * Obtient le nom du visiteur.
     *
     * @return le nom du visiteur
     */
    public String getVisNom() {
        return visNom;
    }

    /**
     * Obtient le prenom du visiteur.
     *
     * @return le prenom du visiteur
     */
    public String getVisPrenom() {
        return visPrenom;
    }

    /**
     * Initialise ou modifie le matricule du visiteur
     *
     * @param visMatricule le matricule du visiteur
     */
    public void setVisMatricule(String visMatricule) {
        this.visMatricule = visMatricule;
    }

    /**
     * Initialise ou modifie le nom du visiteur
     *
     * @param visNom le nom du visiteur
     */
    public void setVisNom(String visNom) {
        this.visNom = visNom;
    }

    /**
     * Initialise ou modifie le prenom du visiteur
     *
     * @param visPrenom le nom du visiteur
     */
    public void setVisPrenom(String visPrenom) {
        this.visPrenom = visPrenom;
    }


    /***
     * Renvoie la description d'un visiteur
     * @return description du visiteur
     */
    @Override
    public String toString() {
        return getClass()+" : [Matricule : "+getVisMatricule()+" , Nom : "+getVisNom()+", Prénom : "+getVisPrenom()+"]";
    }


}