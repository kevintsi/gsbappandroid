package fr.uc.gsb_app.metiers;

import java.util.Date;

/**
 * Le type Rapport visite.
 */
public class RapportVisite {
    private String vis_mat;
    private int rap_num;
    private int pra_num;
    private String pra_nom;
    private String pra_prenom;
    private String bilan;
    private String rap_dateVisite;
    private String rap_dateRapport;

    /**
     * Obtient le nom du praticien.
     *
     * @return le  nom du praticien
     */
    public String getPra_nom() {
        return pra_nom;
    }

    /**
     * Initialise ou modifie le nom du praticien.
     *
     * @param pra_nom le nom du praticien
     */
    public void setPra_nom(String pra_nom) {
        this.pra_nom = pra_nom;
    }

    /**
     * Obtient le prenom du praticien.
     *
     * @return le  prenom du praticien
     */
    public String getPra_prenom() {
        return pra_prenom;
    }

    /**
     * Initialise ou modifie le prenom du praticien.
     *
     * @param pra_prenom le prenom du praticien
     */
    public void setPra_prenom(String pra_prenom) {
        this.pra_prenom = pra_prenom;
    }

    /**
     * Instancie un nouveau Rapport de visite.
     *
     * @param rap_num         le numero de rapport
     * @param pra_num         le numero du praticien
     * @param pra_nom         le nom du praticien
     * @param pra_prenom      le prenom du praticien
     * @param bilan           le bilan du rapport
     * @param rapDateVisite   la date de la visite
     * @param rap_dateRapport la date du rapport de visite
     */
    public RapportVisite(int rap_num, int pra_num, String pra_nom, String pra_prenom, String bilan, String rapDateVisite, String rap_dateRapport) {
        this.rap_num = rap_num;
        this.pra_num = pra_num;
        this.pra_nom = pra_nom;
        this.pra_prenom = pra_prenom;
        this.bilan = bilan;
        this.rap_dateVisite = rapDateVisite;
        this.rap_dateRapport = rap_dateRapport;
    }

    /**
     * Instancie un nouveau Rapport de visite.
     *
     * @param vis_mat       le matricule du visiteur
     * @param pra_num       le numero du praticien
     * @param bilan         le bilan du rapport
     * @param rapDateVisite la date de la visite
     */
    public RapportVisite(String vis_mat, int pra_num, String bilan, String rapDateVisite ) {
        this.vis_mat = vis_mat;
        this.pra_num = pra_num;
        this.bilan = bilan;
        this.rap_dateVisite = rapDateVisite;
    }


    /**
     * Initialise ou modifie le matricule du visiteur.
     *
     * @param vis_mat le matricule du visiteur
     */
    public void setVis_mat(String vis_mat) {
        this.vis_mat = vis_mat;
    }

    /**
     * Initialise ou modifie le numero du rapport de visite.
     *
     * @param rap_num le numero du rapport de visite
     */
    public void setRap_num(int rap_num) {
        this.rap_num = rap_num;
    }

    /**
     * Initialise ou modifie le numero du praticien.
     *
     * @param pra_num le numero du praticiens
     */
    public void setPra_num(int pra_num) {
        this.pra_num = pra_num;
    }

    /**
     * Initialise ou modifie le bilan du rapport de visite.
     *
     * @param bilan le bilan du rapport de visite
     */
    public void setBilan(String bilan) {
        this.bilan = bilan;
    }

    /**
     * Initialise ou modifie la date de la visite.
     *
     * @param rap_dateVisite la date de la visite
     */
    public void setRap_dateVisite(String rap_dateVisite) {
        this.rap_dateVisite = rap_dateVisite;
    }

    /**
     * Initialise ou modifie la date du rapport de visite.
     *
     * @param rap_dateRapport la date du rapport de visite
     */
    public void setRap_dateRapport(String rap_dateRapport) { this.rap_dateRapport = rap_dateRapport; }

    /**
     * Obtient le matricule du visiteur.
     *
     * @return le matricule du visiteur
     */
    public String getVis_mat() {
        return vis_mat;
    }

    /**
     * Obtient le numero du rapport du visite.
     *
     * @return le numero du rapport du visite
     */
    public int getRap_num() {
        return rap_num;
    }

    /**
     * Obtient le numero du praticien.
     *
     * @return le numero du praticien
     */
    public int getPra_num() {
        return pra_num;
    }

    /**
     * Obtient le bilan du rapport de visite.
     *
     * @return le bilan du rapport de visite
     */
    public String getBilan() {
        return bilan;
    }

    /**
     * Obtient la date de la visite.
     *
     * @return la date de la visite
     */
    public String getRap_dateVisite() {
        return rap_dateVisite;
    }

    /**
     * Obtient la date du rapport de visite.
     *
     * @return la date du rapport de visite
     */
    public String getRap_dateRapport() {
        return rap_dateRapport;
    }


    /**
     * Renvoie la description d'un rapport de visite
     * @return la description
     */
    @Override
    public String toString() {
        return "RapportVisite{" +
                "rap_num=" + rap_num +
                ", pra_nom='" + pra_nom + '\'' +
                ", pra_prenom='" + pra_prenom + '\'' +
                ", bilan='" + bilan + '\'' +
                ", rap_dateVisite='" + rap_dateVisite + '\'' +
                ", rap_dateRapport='" + rap_dateRapport + '\'' +
                '}';
    }
}
