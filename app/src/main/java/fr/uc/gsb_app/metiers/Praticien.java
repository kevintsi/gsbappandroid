package fr.uc.gsb_app.metiers;

/**
 * Le type Praticien.
 */
public class Praticien {
    private int praNum;
    private String praNom;
    private String praPrenom;

    /**
     * Instancie un nouweau Praticien.
     *
     * @param num    le num
     * @param nom    le nom
     * @param prenom le prenom
     */
    public Praticien(int num, String nom, String prenom)
    {
        this.praNum = num;
        this.praNom = nom;
        this.praPrenom = prenom;
    }

    /**
     * Obtient le numero du praticien.
     *
     * @return le numero du praticien
     */
    public int getPraNum() {
        return praNum;
    }

    /**
     * Initialise ou modifie le numero du praticien.
     *
     * @param praNum le numero du praticien
     */
    public void setPraNum(int praNum) {
        this.praNum = praNum;
    }

    /**
     * Obtient le nom du praticien.
     *
     * @return le nom du praticien
     */
    public String getPraNom() {
        return praNom;
    }

    /**
     * Initialise ou modifie le nom du praticien.
     *
     * @param praNom le nom du praticien
     */
    public void setPraNom(String praNom) {
        this.praNom = praNom;
    }

    /**
     * Obtient le prenom du praticien.
     *
     * @return le prenom du praticien
     */
    public String getPraPrenom() {
        return praPrenom;
    }

    /**
     * Initialise ou modifie le prenom du praticien.
     *
     * @param praPrenom le prenom du praticien
     */
    public void setPraPrenom(String praPrenom) {
        this.praPrenom = praPrenom;
    }

    /**
     *  Retourne la description d'un praticien
     * @return la description
     */
    @Override
    public String toString() {
        return praNom+"-"+praPrenom;
    }
}
