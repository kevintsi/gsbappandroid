package fr.uc.gsb_app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import fr.uc.gsb_app.metiers.Praticien;
import fr.uc.gsb_app.metiers.RapportVisite;

/**
 * Le type Saisir compte rendu activity.
 */
public class SaisirCompteRenduActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener{
    private DatePicker datePicker;
    private String matricule;
    private Calendar calendar;
    private EditText rv_bilan;
    private Button btn_inputRV;
    private Spinner praticienSpinner;
    private TextView dateView;
    private TextView tvPraticien;
    private ArrayList<Praticien> lesPraticiens = new ArrayList<>();
    private Praticien selectedPraticien;
    private int year, month, day;
    private RapportVisite rapportV;
    private String rapportToString;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saisir_compte_rendu);

        tvPraticien = findViewById(R.id.tvPraticien);
        tvPraticien.setText("Veuillez selectionner un praticien : ");
        btn_inputRV = findViewById(R.id.btn_inputRV);
        btn_inputRV.setText("Valider");
        praticienSpinner = findViewById(R.id.praticienSpinner);
        praticienSpinner.setOnItemSelectedListener(this);
        datePicker = findViewById(R.id.datePicker);
        dateView = findViewById(R.id.et_Date);
        rv_bilan = findViewById(R.id.et_Bilan);



        SharedPreferences su = this.getSharedPreferences("default", 0);
        Log.i("Matricule ", su.getString("matricule",""));

        matricule = su.getString("matricule","");
        Toast.makeText(SaisirCompteRenduActivity.this, "Matricule : " + su.getString("matricule",""),Toast.LENGTH_LONG).show();
        loadPraticien();
    }

    /***
     * Méthode permettant de charger les praticiens
     */
    public void loadSpinnerPraticien()
    {
        ArrayAdapter<Praticien> praticienAdaptateur = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, lesPraticiens);
        praticienAdaptateur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        praticienSpinner.setAdapter(praticienAdaptateur);
    }

    /***
     * Méthode *SERVICE REST* intègre la liste de praticiens
     *
     */
    public void loadPraticien()
    {
        String url = "http://192.168.1.18/gsbWS/web/app.php/lesPraticiens/"+matricule;
        Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("APP_RV"," "+ response.toString());

                try {
                    if (lesPraticiens != null){
                        lesPraticiens.clear();
                    }
                    for(int i = 0; i < response.length() ; i++){
                        String praNum  = response.getJSONObject(i).getString("praNum");
                        String praNom = response.getJSONObject(i).getString("praNom");
                        String praPrenom = response.getJSONObject(i).getString("praPrenom");
                        Praticien lePraticien = new Praticien(Integer.parseInt(praNum),praNom,praPrenom);
                        Log.i("lesPraticiens ----->",lePraticien.toString());
                        lesPraticiens.add(lePraticien);
                    }
                    loadSpinnerPraticien();
                } catch (JSONException e) {
                    Log.e("lesPraticiens", "Erreur JSON: " + e.getMessage());
                }

            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("lesPraticiens", "Erreur HTTP : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        selectedPraticien = (Praticien) praticienSpinner.getSelectedItem();
        Log.i("SelectedPraticien", selectedPraticien.toString());
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    /**
     * Méthode d'envoi du RapportVisite Sérialisé
     *
     * @param view the view
     */
    public void sendRV(View view)
    {

        month = datePicker.getMonth()+1;
        final String dateVisite = datePicker.getDayOfMonth()+"/"+month+"/"+datePicker.getYear();
        Log.i("DATA ", "DATE : "+dateVisite+" BILAN : "+rv_bilan.getText().toString()+" PRATICIEN : "+selectedPraticien.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://192.168.1.18/gsbWS/web/app.php/addRV";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AddPraticien", "Response Listener Début "+response);
                        Toast.makeText(SaisirCompteRenduActivity.this,"Votre rapport a bien été ajouté",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(SaisirCompteRenduActivity.this,SommaireVisiteur.class);
                        startActivity(intent);

                        /*Bundle paquet = new Bundle();
                        paquet.putString("new_praticien",praticienToString);
                        intent.putExtras(paquet);
                        */

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("AddPraticie","Error HTTP " +error.getMessage());
                    }
                }
        )
        {
            /*
                Pour les tests directs avec postman
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/x-www-form-urlencoded");
            */

            /***
             * Convertir l'objet RapportVisite en JSON
             * @return
             */
            @Override
            protected Map<String, String> getParams() {

                //Log.i("DateVisite",rpdateVisite.toString());
                rapportV = new RapportVisite(matricule,selectedPraticien.getPraNum(),rv_bilan.getText().toString(),dateVisite);

                // Sérialisation : Rapport de visite to JsonString
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                rapportToString = gson.toJson(rapportV);

                Log.d("RAPPORTVISITE:", "Après sérialisation"+rapportToString);
                // Send with HTTP POST => create RapportVisite
                Map<String, String> params = new HashMap<>();

                params.put("RapportVisite", rapportToString);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");

                return headers;
            }
        };
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.i("CHANGEDATE",datePicker.getDayOfMonth()+"/"+datePicker.getMonth()+"/"+datePicker.getYear());
    }
}
