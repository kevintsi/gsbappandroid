package fr.uc.gsb_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.uc.gsb_app.metiers.Praticien;
import fr.uc.gsb_app.metiers.RapportVisite;

/**
 * Le type Liste compte-rendu activite.
 */
public class ListeCRActivity extends AppCompatActivity
                             implements OnItemSelectedListener, AdapterView.OnItemClickListener {

    /**
     * La liste view rapport.
     */
    ListView listViewRapport;
    /**
     * Le spinner avec les dates.
     */
    Spinner dateSpinner;
    private String mat;


    private ArrayList<RapportVisite> lesRapports = new ArrayList<RapportVisite>();
    private ArrayList<String> lesDates = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_cr);

        listViewRapport = findViewById(R.id.listeViewCR);
        dateSpinner = findViewById(R.id.dateSpinner);

        SharedPreferences su = this.getSharedPreferences("default", 0);
        mat = su.getString("matricule", "");

        /*for (String uneDate : lesDates) {
            Log.d("Une date ", uneDate);
        }
        Log.d("-------les dates BEFORE", "--------");*/
        //addDatesSpinner();
        Bundle paquet = this.getIntent().getExtras();
        lesDates = paquet.getStringArrayList("lesDates");

        ArrayAdapter<String> dataAdaptateur = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lesDates);
        dataAdaptateur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        dateSpinner.setAdapter(dataAdaptateur);
        dateSpinner.setOnItemSelectedListener(this);
        listViewRapport.setOnItemClickListener(this);
    }




    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        addDatesSpinner(item);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /**
     * Permet de charger la listeView de le RapportVisites
     *
     * @param item the item
     */
    public void addDatesSpinner(String item){
        Log.d("Date --->",item);
        String url = "http://192.168.1.18/gsbWS/web/app.php/rv/listeRapportVisiteDate/"+mat+"/"+item ;
        Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("APPEL WS getLesRV----->"," :"+ response.toString());
                try {
                    lesRapports.clear();
                    for(int i = 0; i < response.length() ; i++){
                        RapportVisite unRapport = new RapportVisite(
                                response.getJSONObject(i).getInt("rapNum"),
                                response.getJSONObject(i).getInt("praNum"),
                                response.getJSONObject(i).getString("praNom"),
                                response.getJSONObject(i).getString("praPrenom"),
                                response.getJSONObject(i).getString("rapBilan"),
                                response.getJSONObject(i).getJSONObject("rapDatevisite").getString("date").split(" ")[0],
                                response.getJSONObject(i).getJSONObject("rapDaterapport").getString("date").split(" ")[0]
                        );
                        lesRapports.add(unRapport);
                        Log.i("RESPONSE :", lesRapports.get(i).toString());
                        ItemRapportVisite adaptateur = new ItemRapportVisite(getApplicationContext(), lesRapports);
                        listViewRapport.setAdapter(adaptateur);
                    }

                } catch (JSONException e) {
                    Log.e("ListeCRActivity", "Erreur JSON: " + e.getMessage());
                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ListeCRActivity", "Erreur HTTP : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

    }

/*

    public void onSubmitRapport(String item){
        String url = "http://192.168.0.14//gsbWS/web/app.php/rv/detailsRapport/"+item ;
        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("APP_RV","Visiteur : "+ response.toString());
                try {
                    int matriculeRapport = response.getInt("rapNum");
                    String nomPra = response.getString("praNom");
                    String prenomPra = response.getString("praPrenom");
                    String bilan = response.getString("rapBilan");
                    String rapDatevisite = response.getJSONObject("rapDatevisite").getString("date").split(" ")[0];
                    String rapDaterapport = response.getJSONObject("rapDaterapport").getString("date").split(" ")[0];

                    //RapportVisite leRapport = new RapportVisite(matriculeRapport, nomPra, prenomPra, bilan, rapDatevisite, rapDaterapport);

                } catch (JSONException e) {
                    Log.e("ListeCRActivity", "Erreur HTTP : " + e.getMessage());
                }
            }

        };


            Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ListeCRActivity", "Erreur HTTP : " + error.getMessage());
                }
            };

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);

    }
    */


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RapportVisite item =(RapportVisite) parent.getItemAtPosition(position);
        Intent intentMenu = new Intent(this, DetailsCompteRenduActivity.class);
        Gson gson = new GsonBuilder().create();
        String jsonCompteRendu = gson.toJson(item);
        Log.d("leRapportVisiteBEFORE->",jsonCompteRendu);
        intentMenu.putExtra("leCompteRendu", jsonCompteRendu);
        startActivity(intentMenu);

    }
}
