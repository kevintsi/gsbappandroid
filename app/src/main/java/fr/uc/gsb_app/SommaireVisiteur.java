package fr.uc.gsb_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.uc.gsb_app.metiers.RapportVisite;

/**
 * Le type Sommaire visiteur.
 */
public class SommaireVisiteur extends AppCompatActivity {

    /**
     * Le Consult compte-rendu.
     */
    Button consultCR;
    /**
     * Le Saisir compte-rendu.
     */
    Button saisirCR;
    /**
     * Le Nom prenom.
     */
    TextView nomPrenom;
    /**
     * Le Role.
     */
    TextView role;

    private ArrayList<String> lesDates = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sommaire__visiteur);
        nomPrenom = (TextView) findViewById(R.id.nomPrenomV);
        role = (TextView) findViewById(R.id.roleV);
        consultCR = (Button) findViewById(R.id.btn_consulterCR);
        saisirCR = (Button) findViewById(R.id.btn_saisirCR);


        SharedPreferences su = this.getSharedPreferences("default", 0);
        String nom = su.getString("nom", "");
        String prenom = su.getString("prenom","");

        nomPrenom.setText(nom.toUpperCase()+" "+prenom);
    }

    /**
     * On inser newt rv.
     *
     * @param view the view
     */
    public void onInserNewtRV(View view)
    {
        Intent intentMenu = new Intent(this, SaisirCompteRenduActivity.class);
        startActivity(intentMenu);
    }

    /**
     * Méthode permettant la récupération des dates
     *
     * @param view the view
     */
    public void onConsultRV(View view){


        SharedPreferences su = this.getSharedPreferences("default", 0);
        String matricule = su.getString("matricule", "");

        String url = "http://192.168.1.18/gsbWS/web/app.php/rv/listeRapportVisite/"+matricule;
        Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("APP_RV"," "+ response.toString());

                try {
                    if (lesDates != null){
                        lesDates.clear();
                    }
                    for(int i = 0; i < response.length() ; i++){
                        String uneDate  = response.getJSONObject(i).getString("dateRapportVisite");
                        lesDates.add(uneDate);
                    }
                    Intent intentMenu = new Intent(SommaireVisiteur.this, ListeCRActivity.class);
                    intentMenu.putExtra("lesDates", lesDates);
                    startActivity(intentMenu);
                } catch (JSONException e) {
                    Log.e("ListeCRActivity", "Erreur JSON: " + e.getMessage());
                }

            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ListeCRActivity", "Erreur HTTP : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                                                                 responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

    }

    /***
     * Méthode prenant le déconnexion du visiteur
     * @param view the view
     */
    public void onDisconnect(View view){
        SharedPreferences su = this.getSharedPreferences("default", 0);
        SharedPreferences.Editor editer = su.edit();
        editer.clear();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);

    }
}
