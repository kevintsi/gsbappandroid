package fr.uc.gsb_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.uc.gsb_app.metiers.RapportVisite;

/**
 * Le type Item rapport visite.
 */
public class ItemRapportVisite extends ArrayAdapter<RapportVisite> {
    /**
     * Le Contexte.
     */
    Context context;
    private  ArrayList<RapportVisite> lesRapports = new ArrayList<RapportVisite>();

    /**
     * Instancie un nouveau Item rapport visite.
     *
     * @param context     le contexte
     * @param lesRapports les rapports
     */
    public ItemRapportVisite(Context context, ArrayList<RapportVisite> lesRapports ){
        super(context, -1, lesRapports);
        this.context = context;
        this.lesRapports = lesRapports;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vItem = null;
        if (convertView != null){
            vItem = convertView;
        }else {
            vItem = layoutInflater.inflate(R.layout.item_rapport_visite, parent, false);
        }

        TextView nomPra =  vItem.findViewById(R.id.nomPra);
        TextView prenomPra = vItem.findViewById(R.id.prenomPra);
        TextView rap_dateRapport = vItem.findViewById(R.id.rap_dateRapport);


        nomPra.setText(lesRapports.get(position).getPra_nom().toUpperCase()+" ");
        prenomPra.setText(lesRapports.get(position).getPra_prenom()+"  Vu le: ");
        rap_dateRapport.setText(lesRapports.get(position).getRap_dateVisite());
        return vItem;
    }
}
