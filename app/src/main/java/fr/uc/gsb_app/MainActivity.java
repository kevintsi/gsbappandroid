package fr.uc.gsb_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Le type Main activite.
 */
public class MainActivity extends AppCompatActivity {
    /**
     * Le Var log.
     */
    EditText varLog;
    /**
     * Le Var mdp.
     */
    EditText varMdp;
    /**
     * Le Submit btn.
     */
    Button submitBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        varLog = (EditText) findViewById(R.id.et_login);
        varMdp = (EditText) findViewById(R.id.et_mdp);

        submitBTN = (Button) findViewById(R.id.submitBTN);
    }

    /**
     * Méthode permettant de:
     * - vérifier l'authenticité du login et du mot de passe
     * - récupérer les coordonées relatives au visiteur connecté
     *
     * @param view the view
     */
    public void onSumbit(View view){
        String login = varLog.getText().toString();
        String mdp  = varMdp.getText().toString();

        ///////////////////// Ajout ////////////////////////////

        String url = "http://192.168.1.18/gsbWS/web/app.php/connexion/" + login + "/" + mdp;
        Log.d("URL->>>>>>", url);

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("APP_RV","Visiteur : "+ response.toString());
                try {
                    String matricule = response.getString("visMatricule");
                    String nom = response.getString("visNom");
                    String prenom = response.getString("visPrenom");
                    connecterVisiteur(matricule, nom, prenom);
                } catch (JSONException e) {
                    Log.e("LOGGED---->",e.getMessage());
                    Toast.makeText(MainActivity.this, "Echec de connexion ", Toast.LENGTH_LONG).show();

                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur HTTP : " + error.getMessage());
            }
        };

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);

        //System.out.println(visiteur.toString());
    }

    /***
     * Fonction permettant de mettre en session les coordonnées qui concerne le visiteur connecter
     * @param matricule the matricule
     * @param nom the nom
     * @param prenom the prenom
     */
    public void connecterVisiteur(String matricule, String nom, String prenom) {

        SharedPreferences su = this.getSharedPreferences("default", 0);;

        SharedPreferences.Editor editer = su.edit();
        editer.putString("matricule", matricule);
        editer.putString("nom", nom);
        editer.putString("prenom", prenom);
        editer.commit();

        Intent intentMenu = new Intent(this, SommaireVisiteur.class);
        startActivity(intentMenu);

        }
    }

