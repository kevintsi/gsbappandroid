package fr.uc.gsb_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;

import fr.uc.gsb_app.metiers.RapportVisite;

/**
 * Le type Details compte rendu activite.
 */
public class DetailsCompteRenduActivity extends AppCompatActivity {

    /**
     * Le Identite praticien.
     */
    TextView identitePraticien;
    /**
     * Le Bilan compte rendu.
     */
    TextView bilanCR;
    /**
     * Le Date compte rendu.
     */
    TextView datesCR;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_compte_rendu);

        identitePraticien = findViewById(R.id.identitePraticien);
        bilanCR = findViewById(R.id.bilanCR);
        datesCR = findViewById(R.id.datesCR);


        String jsonCompteRenduRecup = this.getIntent().getStringExtra("leCompteRendu");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        RapportVisite leRapportVisite = gson.fromJson(jsonCompteRenduRecup, RapportVisite.class);

        identitePraticien.setText("Praticien Visité: "+leRapportVisite.getPra_nom().toUpperCase()+" "+leRapportVisite.getPra_prenom());
        datesCR.setText("Entretien effectué: "+leRapportVisite.getRap_dateVisite()+"\nCompte rendu rédigé le: "+leRapportVisite.getRap_dateRapport());
        bilanCR.setText("Retour(s) concernant l'entretien: \n \n"+leRapportVisite.getBilan());

    }

}

